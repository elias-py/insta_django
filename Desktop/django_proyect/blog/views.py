from django.shortcuts import render
from django.http import HttpResponse

from .models import Post

posts = [
	{
		'author': 'Elias',
		'title': 'blogpost 1',
		'content': 'firstPost Content',
		'date_posted': 'August 21th 2019'
	},
	{
		'author': 'Jame',
		'title': 'blogpost 2',
		'content': 'SecondPost Content',
		'date_posted': 'August 28th 2019'
	}
]

# Create your views here.

def home(request):
	context = {
		'posts': Post.objects.all()
	}
	return render(request, 'blog/home.html', context)


def about(request):
	return render(request, 'blog/about.html', {'title': 'About'})

