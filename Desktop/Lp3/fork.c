#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main() {

    pid_t child_pid;

    printf("the main program proccess ID is %d\n", (int) getpid());

    child_pid = fork();
    if (child_pid != 0) {
        printf("this is the parent proccess, with ID %d\n", (int) getpid());
        printf("the child's proccess ID is %d\n", (int) child_pid);
    } else
        printf("this is de child proccess, whith ID %d\n", (int) getpid());

    return 0;
}