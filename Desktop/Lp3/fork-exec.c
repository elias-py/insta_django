#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int spawn(char* program, char** arg_list) {

    pid_t child_pid;

    /* Duplicar este proceso */
    child_pid = fork();
    if(child_pid != 0)
        /* Este es el proceso padre */
        return child_pid;
    else {
        /* Ahora ejecutar Program, buscandole en el PATH */
        execvp(program, arg_list);
        /* La funcion execvp retorna solo si ocurrio un error */
        fprintf(stderr, "an error has occurred in execvp\n");
        abort();
    }
}

int main() {

    char* arg_list[] = {
        "ls",
        "-l",
        "/",
        NULL
    };

    spawn("ls", arg_list);

    printf("done with main program\n");

    return 0;
}