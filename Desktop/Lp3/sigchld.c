#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

sig_atomic_t child_exist_status(int signal_number) {

    /* Limpia el proceso hijo */
    int status;
    wait(&status);
    /* Guarda el status existente en una variable global */
    child_exist_status = status;

}

int main() {
    
    struct sigaction sigchld_action;
    memset(&sigchld_action, 0, sizeof(sigchld_action));
    sigchld_action.sa_handler = &clean_up_child_process;
    sigaction (SIGCHLD, &sigchld_action, NULL);

    return 0;

}