#include <stdio.h>
#include <unistd.h>

/* Un identificador para un archivo temporal creado con write_temp_file. 
Esta implementación, es solo un descriptor de archivo. */
typedef int temp_file_handle;

/* Escribe bytes de LONGITUD desde BUFFER en un archivo temporal. 
El archivo temporal se desvincula inmediatamente. 
Devuelve un identificador al archivo temporal. */

temp_file_handle write_temp_file(char* buffer, size_t length) {

	/* Crear el nombre de archivo y el archivo. El XXXXXX será 
	reemplazado por caracteres que hacen que el nombre del archivo sea único. */
	char temp_filename[] = "/tmp/temp_file.XXXXXX";
	int fd = mkstemp(temp_file);
	/* Desvincular el archivo inmediatamente, para que se elimine 
	cuando se cierre el descriptor de archivo. */
	unlink(temp_filename);
	/* Escribir el numero de bytes al archivo primero. */
	write(fd, &length, sizeof(length));
	/* Ahora escribe los datos en sí. */
	write(fd, buffer, length);
	/* Utilice el descriptor de archivo como el identificador para el archivo temporal. */
	return fd;
}

/* Lee el contenido de un archivo temporal TEMP_FILE creado con write_temp_file. 
El valor de retorno es un búfer recientemente asignado de esos contenidos, que la 
persona que llama debe desasignar gratis. * LONGITUD se establece en el tamaño de 
los contenidos, en bytes. El archivo temporal se elimina. */

char* reade_temp_file(temp_file_handle temp_file, size_t* length) {

	char* buffer;
	/* El identificador TEMP_FILE es un descriptor de archivo para el archivo temporal. */
	int fd = temp_file;
	/* Rebobinar hasta el comienzo del archivo. */
	lseek(fd, 0, SEEK_SET);
	/* Leer el tamaño de los datos en el archivo temporal. */
	read(fd, length, sizeof(*length));
	/* Asignar un búfer y leer los datos. */
	buffer = (char*) malloc(*length);
	read(fd, buffer, *length);
	/* Cierre el descriptor de archivo, lo que hará que el archivo temporal desaparezca. */
	close(fd);
	return buffer;
}

return 0;
