#include <fcntl.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

char* read_from_file(const char* filename, size_t length) {
    
    char* buffer;
    int fd;
    ssize_t bytes_read;

    /* Alocar el buffer */
    buffer = (char*) malloc (length);
    if (buffer == NULL)
        return NULL;
    /* Abrir el archivo */
    fd = open (filename, O_RDONLY);
    if (fd == -1) {
        /* Error al abrir, liberar el buffer antes de retornar */
        free (buffer);
        return NULL;
    }
    /* Leer los datos */
    bytes_read = read(fd, buffer, length);
    if (bytes_read != length) {
        /* Lectura fallida, liberar el buffer y cerrar fd antes de retornar */
        free(buffer);
        close(fd);
        return NULL;
    }
    /* Todo esta bien. Cerrar el archivo y retornar el buffer */
    close(fd);
    return buffer;
}